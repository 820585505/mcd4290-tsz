function doIt()
{
    let num1Ref;
    let num1;
    let num2Ref;
    let num2;
    let num3Ref;
    let num3;
    let answerRef;
    let ans;
    let oddEvenRef;
    
    num1Ref = document.getElementById("number1");
    num1 = Number(num1Ref.value);
    
    num2Ref = document.getElementById("number2");
    num2 = Number(num2Ref.value);
    
    num3Ref = document.getElementById("number3");
    num3 = Number(num3Ref.value);

    answerRef = document.getElementById("answer");
    
    ans = parseInt(num1)+parseInt(num2)+parseInt(num3);
    answerRef.innerText = ans;
    
    oddEvenRef = document.getElementById("oddEven");
    
    if(ans>0)
        {
            answerRef.className ="positive";
        }
    else
        {
            answerRef.className ="negative";
        }
        
        
    if(ans%2 === 0)
        {
        oddEvenRef.innerHTML = "even";    
        oddEvenRef.className = "even";
        }
    else{
        oddEvenRef.innerHTML = "odd";
        oddEvenRef.className = "odd";
    }
        
}