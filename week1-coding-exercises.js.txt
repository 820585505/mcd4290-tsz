// Question 1
let radius = 4;
let pi = Math.PI;
let circumference = 2*radius*pi;
let C = circumference.toFixed(2);
console.log(C);

// Question 2 
let animalString = "cameldogcatlizard";
let andString = " and ";
let a = animalString.substring(8,11); // Returns "cat"
let b = animalString.substr(5,3); // Returns "dog"
let c = andString.substr(1,3); // Returns "and"
console.log(a+" "+c+" "+b); // space method 1 
console.log(`${a} ${c} ${b}`); // space method 2

// Question 3
// create an object with four properties
let Object = {
  firstN : "Kanye",
  lastN : "West",
  Birth : "8 June 1977",
  AnI: 15e7,
}
console.log(Object.firstN+" "+Object.lastN+" was born "+Object.Birth+" and has an annual income of "+Object.AnI )

// Question 4
var number1, number2, number3;
//RHS generates a random number between 1 and 10 inclusive
number1 = Math.floor((Math.random() * 10) + 1);
//RHS generates a random number between 1 and 10 inclusive
number2 = Math.floor((Math.random() * 10) + 1);
console.log("number1 = " + number1 + " number2 = " + number2);
//HERE your code to swap the values in number1 and number2
number3 = number1;
number1 = number2;
number2 = number3;
console.log("number1 = " + number1 + " number2 = " + number2);

// Question 5
let year;
let yearNot2015Or2016;
year = 2015;
yearNot2015Or2015 = year !==2015 && year!==2016;
console.log(yearNot2015Or2015);
